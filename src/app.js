import fetch from 'node-fetch'
import dotenv from 'dotenv'
import BootBot from './helpers/BootBotFolder/BootBot'

const GIPHY_URL = `http://api.giphy.com/v1/gifs/random?api_key=wyVJaC5ldHvuYTNHK1bcMD7Tmc9DzHpB&tag=`

// eslint-disable-next-line no-undef
dotenv.config()

const bot = new BootBot({
  accessToken: process.env.BOT_ACCESS_TOKEN,
  verifyToken: process.env.VERIFY_TOKEN,
  appSecret: process.env.APP_SECRET,
})

bot.hear(['hello', 'hi', 'hey'], (payload, chat) => {
  chat.say(
    'Привет! Я бот компании Ботс-Бизнесс, приятно познакомится. Нажми на кнопку или напиши слово "Анимация".',
  )
})

bot.hear(/Анимация (.*)/i, (payload, chat, data) => {
  const query = data.match[1]
  chat.say('Searching for the perfect gif...')
  fetch(GIPHY_URL + query)
    .then(res => res.json())
    .then(json => {
      chat.say(
        {
          attachment: 'image',
          url: json.data.image_url,
        },
        {
          typing: true,
        },
      )
    })
})

bot.start(4001)
